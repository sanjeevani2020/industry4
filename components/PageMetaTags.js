export default function PageMetaTags()
{
    return(
        <>

            <title>Industry 4.0</title>
            <meta name="description" content="Industry4" />
            <meta name="keywords" content="Industry4" />
            <link rel="canonical" href="https://industry4.vercel.app/" />
            <meta name="robots" content="index, follow" />

        </>
    )
}