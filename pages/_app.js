import '../styles/globals.css'


import { useEffect } from "react";

// Modal.setAppElement('#root');

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    if("serviceWorker" in navigator) {
      window.addEventListener("load", function () {
       navigator.serviceWorker.register("/sw.js").then(
          function (registration) {
            console.log("Service Worker registration successful with scope: ", registration);
            if (registration.installing) {
              console.log('SW installing');
            } else if (registration.waiting) {
              console.log('SW waiting');
            } else if (registration.active) {
              console.log('SW activated');
            }
          },
          function (err) {
            console.log("Service Worker registration failed: ", err);
          }
        );
      });
    }
  }, [])

  

  return(
      
      <Component {...pageProps} />
      
  )
}

export default MyApp
