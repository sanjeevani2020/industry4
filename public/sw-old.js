var cacheName = 'I4';
var filesToCache = [
  '/',
  '/index.js',
  '/css/style.css',
  '/_app.js',
  '/js/bootstrap.min.js',
  '/js/jquery-1.10.2.js'
];

/* Start the service worker and cache all of the app's content */
self.addEventListener('install', function(e) {
  console.log(e);
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});



self.addEventListener('activate', (event) => {
  console.log(e);
  event.waitUntil((async () => {
      if ('navigationPreload' in self.registration) {
      await self.registration.navigationPreload.enable();
    }
  })());
  // Tell the active service worker to take control of the page immediately.
  self.clients.claim();
});

/* Serve cached content when offline */
self.addEventListener('fetch', function(e) {
  console.log(e, "fetch")

  // Ignore crossdomain requests
 if (!e.request.url.startsWith(self.location.origin)) {
  return;
}

// Ignore non-GET requests
if (e.request.method !== 'GET') {
  return;
}

 // Ignore browser-sync
 if (e.request.url.indexOf('browser-sync') > -1) {
  return;
}

 // Prevent index route being cached
 if (e.request.url === (self.location.origin + '/')) {
  return;
}

  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});