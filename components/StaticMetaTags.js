export default function StaticMetaTags()
{
    return(
        <>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0" />
            <meta charSet="utf-8" />



            <link rel="manifest" href="/manifest.json"/>


            
{/* Link Tags */}
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-io/favicon-16x16.png" />
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-io/favicon-32x32.png" />
<link rel="icon" type="image/png" sizes="36x36" href="/favicon-io/favicon-36x36.png" />                          
<link rel="icon" type="image/png" sizes="48x48" href="/favicon-io/favicon-48x48.png"   />
<link rel="icon" type="image/png" sizes="72x72" href="/favicon-io/favicon-72x72.png"   />
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-io/favicon-96x96.png"/>
<link rel="icon" type="image/png" sizes="144x144" href="/favicon-io/favicon-144x144.png"/>
<link rel="icon" type="image/png" sizes="192x192"  href="/favicon-io/favicon-192x192.png"/>




{/*  iOS   */}
<link href="/favicon-io/touch-icon-iphone.png" rel="apple-touch-icon" sizes="180x180" />
<link href="/favicon-io/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76" />
<link href="/favicon-io/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120" />
<link href="/favicon-io/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152" />

<meta name="mobile-web-app-capable" content="yes" />

         
<meta name="full-screen" content="yes"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="mobile-web-app-capable" content="yes"/>            


<link rel="apple-touch-icon" sizes="57x57" href="/favicon-io/apple-icon-57x57.png"/>
<link rel="apple-touch-icon" sizes="60x60" href="/favicon-io/apple-icon-60x60.png"/>
<link rel="apple-touch-icon" sizes="72x72" href="/favicon-io/apple-icon-72x72.png"/>
<link rel="apple-touch-icon" sizes="76x76" href="/favicon-io/apple-icon-76x76.png"/>
<link rel="apple-touch-icon" sizes="114x114" href="/favicon-io/apple-icon-114x114.png"/>
<link rel="apple-touch-icon" sizes="120x120" href="/favicon-io/apple-icon-120x120.png"/>
<link rel="apple-touch-icon" sizes="144x144" href="/favicon-io/apple-icon-144x144.png"/>
<link rel="apple-touch-icon" sizes="152x152" href="/favicon-io/apple-icon-152x152.png"/>
<link rel="apple-touch-icon" sizes="180x180" href="/favicon-io/apple-icon-180x180.png"/>
<link rel="apple-touch-startup-image" sizes="320x480" href="/favicon-io/touch-icon-start-up-320x480.png" />




{/* Windows */}
<meta name="msapplication-TileImage" content="/favicon-io/ms-icon-144x144.png" />
<meta name="msapplication-config" content="/browserconfig.xml" />

{/* <!-- imagemode - show image even in text only mode  --> */}
<meta name="imagemode" content="force" />

{/* <!-- Disable night mode for this page  -->               */}
<meta name="nightmode" content="enable/disable" /> 





{/* <!-- Tap highlighting  --> */}
<meta name="msapplication-tap-highlight" content="no"/>

{/* <!-- UC Mobile Browser  -->  */}
<meta name="full-screen" content="yes"/>
<meta name="browsermode" content="application"/>
<meta name="nightmode" content="enable/disable"/>
<meta name="viewport" content="uc-fitscreen=yes"/>
<meta name="layoutmode" content="fitscreen/standard" /> 




{/* <!-- imagemode - show image even in text only mode  --> */}
<meta name="imagemode" content="force"/> 





















            
            {/* <link rel="manifest" href="/manifest.json"/> */}


            
              {/* Link Tags */}
              {/* <link rel="icon" type="image/png" sizes="16x16" href="/favicon-io/favicon-16x16.png" />
              <link rel="icon" type="image/png" sizes="32x32" href="/favicon-io/favicon-32x32.png" />
              <link rel="icon" type="image/png" sizes="36x36" href="/favicon-io/favicon-36x36.png" />                          
              <link rel="icon" type="image/png" sizes="48x48" href="/favicon-io/favicon-48x48.png"   />
              <link rel="icon" type="image/png" sizes="72x72" href="/favicon-io/favicon-72x72.png"   />
              <link rel="icon" type="image/png" sizes="96x96" href="/favicon-io/favicon-96x96.png"/>
              <link rel="icon" type="image/png" sizes="144x144" href="/favicon-io/favicon-144x144.png"/>
              <link rel="icon" type="image/png" sizes="192x192"  href="/favicon-io/favicon-192x192.png"/>
              

 */}

            {/*  iOS   */}
            {/* <link href="/favicon-io/touch-icon-iphone.png" rel="apple-touch-icon" sizes="180x180" />
            <link href="/favicon-io/touch-icon-ipad.png" rel="apple-touch-icon" sizes="76x76" />
            <link href="/favicon-io/touch-icon-iphone-retina.png" rel="apple-touch-icon" sizes="120x120" />
            <link href="/favicon-io/touch-icon-ipad-retina.png" rel="apple-touch-icon" sizes="152x152" />

            <meta name="mobile-web-app-capable" content="yes" />

                       
            <meta name="full-screen" content="yes"/>
            <meta name="apple-mobile-web-app-capable" content="yes"/>
            <meta name="mobile-web-app-capable" content="yes"/>            
 
            
            <link rel="apple-touch-icon" sizes="57x57" href="/favicon-io/apple-icon-57x57.png"/>
            <link rel="apple-touch-icon" sizes="60x60" href="/favicon-io/apple-icon-60x60.png"/>
            <link rel="apple-touch-icon" sizes="72x72" href="/favicon-io/apple-icon-72x72.png"/>
            <link rel="apple-touch-icon" sizes="76x76" href="/favicon-io/apple-icon-76x76.png"/>
            <link rel="apple-touch-icon" sizes="114x114" href="/favicon-io/apple-icon-114x114.png"/>
            <link rel="apple-touch-icon" sizes="120x120" href="/favicon-io/apple-icon-120x120.png"/>
            <link rel="apple-touch-icon" sizes="144x144" href="/favicon-io/apple-icon-144x144.png"/>
            <link rel="apple-touch-icon" sizes="152x152" href="/favicon-io/apple-icon-152x152.png"/>
            <link rel="apple-touch-icon" sizes="180x180" href="/favicon-io/apple-icon-180x180.png"/>
            <link rel="apple-touch-startup-image" sizes="320x480" href="/favicon-io/touch-icon-start-up-320x480.png" />



 */}
            {/* Windows */}
            {/* <meta name="msapplication-TileImage" content="/favicon-io/ms-icon-144x144.png" />
            <meta name="msapplication-config" content="/browserconfig.xml" />
            */}
            {/* <!-- imagemode - show image even in text only mode  --> */}
            {/* <meta name="imagemode" content="force" /> */}

            {/* <!-- Disable night mode for this page  -->               */}
            {/* <meta name="nightmode" content="enable/disable" /> */}


            

            
            {/* <!-- Tap highlighting  --> */}
            {/* <meta name="msapplication-tap-highlight" content="no"/> */}

            {/* <!-- UC Mobile Browser  -->  */}
            {/* <meta name="full-screen" content="yes"/>
            <meta name="browsermode" content="application"/>
            <meta name="nightmode" content="enable/disable"/>
            <meta name="viewport" content="uc-fitscreen=yes"/>
            <meta name="layoutmode" content="fitscreen/standard" /> */}




            {/* <!-- imagemode - show image even in text only mode  --> */}
            {/* <meta name="imagemode" content="force"/> */}

            



            

        </>
    )

}