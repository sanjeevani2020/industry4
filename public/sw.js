const cacheName = 'FinalePWA94'
const cacheAssets = ["/",
    // "http://localhost:3000/api/_app.js",
    "/.next/static/chunks/pages/index.js",
    "http://localhost:3000/first.html",
    // "http://localhost:3000/_next/static/chunks/pages/_app.js",
    // "http://localhost:3000/_next/static/chunks/pages/index.js",
    // "/_next/static/chunks/pages/_app.js",
    // "https://localhost:3000/_next/static/chunks/pages/_app-ef4641c4fce4c40230f9.js",
    // "https://industry4.vercel.app/_next/static/chunks/pages/_app-ef4641c4fce4c40230f9.js",
    // "http://localhost:3000/manifest.json",
    // "http://localhost:3000/styles/global.css",
    //  "http://localhost:3000/styles/Home.module.css",
    "http://localhost:3000/favicon.ico"
    
    ]
    
    // '_app.js',
    // 'index.js'



//Call install event
self.addEventListener('install', function(e) {

    console.log('Service Worker : Installed');
    e.waitUntil(
        caches  
            .open(cacheName)
            .then(cache => {
                console.log('Service Worker : Cacheing Files');
                cache.addAll(cacheAssets)
            })
            .then(()=> self.skipWaiting())
    );
    self.skipWaiting()
})

//Call activate event
self.addEventListener('activate', e => {

    console.log('Servcie Worker : Activated')

    e.waitUntil(
        caches.keys()
            .then(cacheNames =>{
                return Promise.all(
                    cacheNames.map(cache => {
                        if(cache !== cacheName){
                            console.log('Service Worker : Clearing old cache')
                            return caches.delete(cache)
                        }
                    })
                );
            })
    )
})


//Call fetch event (this is what offline is)
self.addEventListener('fetch', e => {
    console.log('Service Worker : fetching')
    // e.respondWith(fetch(e.request).catch(() => cathes.match(e.request)));

    // e.respondWith(
    //     fetch(e.request)
    //         .then(response => {
    //             //make copy/clone of response
    //             const resClone = response.clone();
    //             //Open cache
    //             caches
    //                 .open(cacheName)
    //                 .then(cache => {
    //                     //Add response to cache
    //                     cache.put(e.request , resClone);
    //                 }) ;
    //                 return response;
    //         })
    //         .catch(err => caches.match(e.request).then(res => response))

    // )

})




